<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'f_name' => 'Green',
            'l_name' => 'Ink',
            'email' => 'greeninksolutionandtech@gmail.com',
            'password' => Hash::make('GreenInk2019!')
        ]);
    }
}
